﻿

namespace Model
{
    /// <summary>
    /// This class is designed to be a mathematic library
    /// </summary>
    public class Maths
    {
        /// <summary>
        /// This method is designed to addition two integers
        /// </summary>
        /// <param name="op1">First operand</param>
        /// <param name="op2">Second operand</param>
        /// <returns>The result, in integer</returns>
        public int Addition(int op1, int op2)
        {
            return op1 + op2;
        }
    }
}
