# Calculator

Est un petit projet permettant aux développeurs juniors de se réactiver leurs compétences en C#.
Il est présenté comme exercice de réactivation avant d'aborder les bases de la POO.

## Installation

Après avoir récupéré le dépôt, une compilation sous Visual Studio Entreprise 2019 permettra d'avoir une application fonctionnelle.

[Explication en vidéo](https://www.youtube.com/watch?v=Gur5vE0ELoE&feature=youtu.be&hd=1)


## Etape de réalisation

[Externaliser la fonction "Addition" dans un modèle](http://youtu.be/EetLvboTuCE?hd=1)



## Licence
[Licence](Licence.md)